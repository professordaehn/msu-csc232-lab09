/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file FibCalculator.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Specification of FibCalculator interface.
*/

#ifndef LAB09_FIB_CALCULATOR_H__
#define LAB09_FIB_CALCULATOR_H__

/**
 * A calculator that computes values related to the Fibonacci sequence.
 */
class FibCalculator {
public:
    /**
     * Calculate the nth Fibonacci number.
     *
     * @pre n >= 0
     * @param n an index into the Fibonaaci sequence beginning at 0.
     * @return The nth number in the Fibonacci sequence is returned.
     */
    virtual int nthFibonacciNumber(int n) const = 0;
    //virtual ~FibCalculator() {}
};

#endif // LAB09_FIB_CALCULATOR_H__
